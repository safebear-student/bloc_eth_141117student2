pragma solidity ^0.4.11;

import "ds-test/test.sol";

import "./Safevote.sol";

contract SafevoteTest is DSTest {
    Safevote safevote;

    function setUp() public {
        bytes32[] cl;
        safevote = new Safevote(cl);
    }

    function testFail_basic_sanity() public {
        assertTrue(false);
    }

    function test_basic_sanity() public {
        assertTrue(true);
    }
}
